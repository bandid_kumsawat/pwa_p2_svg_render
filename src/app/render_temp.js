var moment = require("moment");

export default {
    content_svg_by_station(obj, station) {
        var content = ''
        for (var i = 0;i < obj.length;i++){
            if (station === obj[i].station){
                content = obj[i].value.svgcontent
            }
        }
        if (content !== ''){
            return content
        }else {
            return 'Not Found Station'
        }
    },
    item_by_station(obj, station){
        var content = ''
        for (var i = 0;i < obj.length;i++){
            if (station === obj[i].station){
                content = obj[i].value.items
            }
        }
        if (content !== ''){
            return content
        }else {
            return 'Not Found Station'
        }
    },
    map(payload, type, svgData){
        // svgExtValue: "svg-ext-value",
        // svgExtHtmlButton: "svg-ext-html_button",
        // svgExtHtmlSelect: "svg-ext-html_select",
        // svgExtHtmlChart: "svg-ext-html_chart",
        // svgExtGaugeSemaphore: "svg-ext-gauge_semaphore"
        // console.log(type.svgExtValue)
        // console.log('+++++++++++++++++++++++++++++++++++++++++++++++++++++')
        // console.log(payload)
        // console.log('+++++++++++++++++++++++++++++++++++++++++++++++++++++')
        // console.log(svgData)
        // console.log('+++++++++++++++++++++++++++++++++++++++++++++++++++++')
        payload.forEach(function (item, index, arr) {
            for (var m = 0;m < svgData.length;m++){
                for (const [key, value] of Object.entries(svgData[m].value.items)) {
                    // console.log("debug --- ")
                    // console.log(item)
                    // console.log(item.tag + " : " + value.name)
                    // console.log(type.svgExtValue + " : " + value.type)
                    // console.log("debug --- ")
                    if (item.tag === value.name && value.type === type.svgExtValue){
                        // console.log("value.id = " + value.id)
                        document.getElementById(value.id).children[0].textContent = item.val
                        //moment(item.value).format('MMMM DD YYYY, HH:mm:ss');
                    }

                    if (item.tag === value.name && value.type === type.svgExtGaugeSemaphore){
                        // // console.log(item)
                        // // console.log(value)
                        value.property.ranges.forEach(function (inArr, i){
                            
                            // // console.log("%c value_ "  + value.id , "color: red;font-size: 30px; font-weight: 700;")
                            var value_ = parseFloat(Number(item.val));
                            var min = parseFloat(inArr.min);
                            var max = parseFloat(inArr.max);
                            console.log("%c value_ "  + value_ , "color: red;font-size: 30px; font-weight: 700;")
                            // // console.log("%c min   "  + min , "color: red;font-size: 30px; font-weight: 700;")
                            // // console.log("%c max   "  + max , "color: red;font-size: 30px; font-weight: 700;")
                            // // console.log({value_, min, max, i})
                            if (i == 0){
                                // console.log("%c i == 0" , "color: #00ff22;font-size: 30px; font-weight: 700;")
                                // console.log("%c value_ "  + value_ , "color: red;font-size: 30px; font-weight: 700;")
                                // console.log("%c min   "  + min , "color: red;font-size: 30px; font-weight: 700;")
                                // console.log("%c max   "  + max , "color: red;font-size: 30px; font-weight: 700;")
                                // console.log("%c value_ >= min && value_ <= max   "  + (value_ >= min && value_ <= max) , "color: red;font-size: 30px; font-weight: 700;")
                                if (value_ >= min && value_ <= max || value_ == min == max){
                                    document.getElementById(value.id).children[0].setAttribute("fill",inArr.color);
                                    // console.log("%c value_ "  + value.id , "color: pink;font-size: 30px; font-weight: 700;")
                                } 
                            }else{
                                // console.log("%c i != 0" , "color: #00ff22;font-size: 30px; font-weight: 700;")
                                // console.log("%c value_ "  + value_ , "color: red;font-size: 30px; font-weight: 700;")
                                // console.log("%c min   "  + min , "color: red;font-size: 30px; font-weight: 700;")
                                // console.log("%c max   "  + max , "color: red;font-size: 30px; font-weight: 700;")
                                // console.log("%c value_ > min && value_ <= max   "  + (value_ > min && value_ <= max) , "color: red;font-size: 30px; font-weight: 700;")
                                if (value_ > min && value_ <= max || value_ == min == max){
                                    document.getElementById(value.id).children[0].setAttribute("fill",inArr.color);
                                    // console.log("%c value_ "  + value.id , "color: blue;font-size: 30px; font-weight: 700;")
                                }
                            }
                        });
                    }

                    
                    // check object chage color
                    // if (value.type == this.svgOjbType.svgExtGaugeSemaphore && value.name === element.tag){
                    //     value.property.ranges.forEach(function (inArr, i){
                    //         var value_ = parseFloat(element.value);
                    //         var min = parseFloat(inArr.min);
                    //         var max = parseFloat(inArr.max);
                    //         if (i == 0){
                    //         if (value_ >= min && value_ <= max){
                    //             document.getElementById(value.id).children[0].setAttribute("fill",value.property.ranges[i].color);
                    //         }
                    //         }else{
                    //         if (value_ > min && value_ <= max){
                    //             document.getElementById(value.id).children[0].setAttribute("fill",value.property.ranges[i].color);
                    //         }
                    //         }
                    //     })
                    // }


                }
            }
// for (const [key, value] of Object.entries(svgData[index].value.items)) {
//     // // console.log(item.tag === value.name)
//     // console.log(item.tag + ' ' +value.name)
//     // // console.log(value.name)
//     /**
//      * item  = payload
//      * value = svgData
//      */
//     // if (item.tag === value.name && value.type === type.svgExtValue){
//     //     // // console.log(item)
//     //     document.getElementById(value.id).children[0].textContent = item.val//moment(item.value).format('MMMM DD YYYY, HH:mm:ss');
//     // }else{
//     //     // document.getElementById(value.id).children[0].textContent = item.value;
//     // }
// }
            // if (item.tag === "date"){
            //     document.getElementById(value.id).children[0].textContent = moment(payload[i].value).format('MMMM DD YYYY, HH:mm:ss');
            // }else{
            //     document.getElementById(value.id).children[0].textContent = element.value;
            // }
        });
    //     //check value
    //     if (value.type == this.svgOjbType.svgExtValue && value.name === element.tag){
    //     if (element.tag === "date"){
    //         document.getElementById(value.id).children[0].textContent = moment(element.value).format('MMMM DD YYYY, HH:mm:ss');
    //     }else{
    //         document.getElementById(value.id).children[0].textContent = element.value;
    //     }
    //     }

    //     // check object chage color
    //     if (value.type == this.svgOjbType.svgExtGaugeSemaphore && value.name === element.tag){
    //     value.property.ranges.forEach(function (inArr, i){
    //         var value_ = parseFloat(element.value);
    //         var min = parseFloat(inArr.min);
    //         var max = parseFloat(inArr.max);
    //         if (i == 0){
    //         if (value_ >= min && value_ <= max){
    //             document.getElementById(value.id).children[0].setAttribute("fill",value.property.ranges[i].color);
    //         }
    //         }else{
    //         if (value_ > min && value_ <= max){
    //             document.getElementById(value.id).children[0].setAttribute("fill",value.property.ranges[i].color);
    //         }
    //         }
    //     })
    //     }
    }
}
  