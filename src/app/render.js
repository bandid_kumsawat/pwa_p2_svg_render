var moment = require("moment");

export default {
    content_svg_by_station(obj, station) {
        var content = ''
        for (var i = 0;i < obj.length;i++){
            if (station === obj[i].station){
                content = obj[i].value.svgcontent
            }
        }
        if (content !== ''){
            return content
        }else {
            return 'Not Found Station'
        }
    },
    item_by_station(obj, station){
        var content = ''
        for (var i = 0;i < obj.length;i++){
            if (station === obj[i].station){
                content = obj[i].value.items
            }
        }
        if (content !== ''){
            return content
        }else {
            return 'Not Found Station'
        }
    },
    map(payload, type, svgData, station_name){
        console.log("Debug.....")
        console.log(type)
        console.log("svgData")
        console.log(svgData)
        payload.forEach(function (item, index, arr) {
            for (var m = 0;m < svgData.length;m++){
                if (station_name === svgData[m].station){
                    for (const [key, value] of Object.entries(svgData[m].value.items)) {
                        if (item.tag === value.name && value.type === type.svgExtValue){
                            document.getElementById(value.id).children[0].textContent = item.val
                        }

                        if (item.tag === value.name && value.type === type.svgExtGaugeSemaphore){
                            value.property.ranges.forEach(function (inArr, i){
                                var value_ = parseFloat(Number(item.val));
                                var min = parseFloat(inArr.min);
                                var max = parseFloat(inArr.max);
                                // console.log("%c value_ "  + value_ , "color: red;font-size: 30px; font-weight: 700;")
                                if (i == 0){
                                    if (value_ >= min && value_ <= max || value_ == min == max){
                                        document.getElementById(value.id).children[0].setAttribute("fill",inArr.color);
                                    } 
                                }else{
                                    if (value_ > min && value_ <= max || value_ == min == max){
                                        document.getElementById(value.id).children[0].setAttribute("fill",inArr.color);
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    }
}
  