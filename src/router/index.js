import Vue from 'vue'
import Router from 'vue-router'

// import component
import renderSvg from '@/components/renderSvg/renderSvg'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/:station_name/",
      name: "renderSvg",
      component: renderSvg
    }
  ]
})
