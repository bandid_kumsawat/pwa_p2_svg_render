export default {
  SET_SVG_DATA (state, data) {
    state.svg_data = data
  },
  SET_DEVICE_NAME (state, data){
    state.device_name = data
  },
  SET_STATION_NAME (state, data){
    state.station_name = data
  },
  GET_TYPE_VALUE () {
    return svgOjbType.svgExtValue
  },
  GET_TYPE_BUTTON () {
    return svgOjbType.svgExtHtmlButton
  },
  GET_TYPE_SELECT () {
    return svgOjbType.svgExtHtmlSelect
  },
  GET_TYPE_CHART() {
    return svgOjbType.svgExtHtmlChart
  },
  GET_TYPE_gauge_semaphore () {
    return svgOjbType.svgExtGaugeSemaphore
  },
}
