export default {
  svg_data: Object,
  device_name: '',
  station_name: '',
  svgOjbType: {
    svgExtValue: "svg-ext-value",
    svgExtHtmlButton: "svg-ext-html_button",
    svgExtHtmlSelect: "svg-ext-html_select",
    svgExtHtmlChart: "svg-ext-html_chart",
    svgExtGaugeSemaphore: "svg-ext-gauge_semaphore"
  },
  time_interval: 2000,
  socket_host: 'http://localhost:4000'
}
