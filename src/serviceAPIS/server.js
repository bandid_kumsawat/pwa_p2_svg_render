import axios from 'axios'

export default () => {
  var api = axios.create({
    baseURL: 'http://34.69.243.34:3000/'
  })
  return api
}
